﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace TicketMachine.Interfaces
{
    public abstract class Suggestion
    {
        public virtual List<char> NextLetter { get; set; }
        public virtual IEnumerable<string> Stations { get; set; }

        public virtual bool IsNullOrEmpty() => this.Stations == null || this.Stations.Count() == 0;
        public virtual string ResultsToString()
        {
            var ConcatenateResults = new StringBuilder();
            if (this.IsNullOrEmpty())
            {
                ConcatenateResults.AppendLine("Results: 0");
                ConcatenateResults.AppendLine("No Matches Found..");
            }
            else
            {
                ConcatenateResults.AppendLine($"Results: {this.Stations.Count()}");
                ConcatenateResults.AppendLine(Environment.NewLine);
                foreach (var station in this.Stations)
                    ConcatenateResults.AppendLine(station);
            }

            return ConcatenateResults.ToString();
        }
    }
}

