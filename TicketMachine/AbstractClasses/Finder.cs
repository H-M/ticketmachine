﻿using System;
using System.Linq;
using System.Globalization;
using TicketMachine.DStructures;
using System.Collections.Generic;

namespace TicketMachine.Interfaces
{
    public abstract class Finder
    {
        public Dictionary<char, HashSet<string>> mappedStations;
        public virtual Suggestions GetSuggestions(string userInput)
        {
            var stringFormatter = new CultureInfo(CultureInfo.CurrentCulture.Name, false).TextInfo;
            var key = Char.ToLower((String.IsNullOrEmpty(userInput) ? ' ' : userInput[0]));
            var input = stringFormatter.ToTitleCase(userInput);
            if (userInput.Length > 0 && mappedStations.ContainsKey(key)) {
                var result = mappedStations[key].Where(station => station.Contains(input) || station.Equals(input)).AsEnumerable();
                if (result.Count() < 1)
                    return new Suggestions(userInput?.ToList<char>(), null);
                else return new Suggestions(userInput?.ToList<char>(), result);
                // return new Suggestions(userInput?.ToList<char>(), result.Count() < 1 ? null : result);
            } else return new Suggestions(userInput?.ToList<char>(), null);
        }
        public virtual void Set(StationsFinder finder) => mappedStations = finder.mappedStations;
    }
}
