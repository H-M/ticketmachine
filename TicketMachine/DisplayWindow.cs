﻿using System;
using System.Windows.Forms;
using TicketMachine.DStructures;

namespace TicketMachine
{
    public partial class DisplayWindow : Form
    {
        private StationsFinder Finder;

        public DisplayWindow()
        {
            Finder = new StationsFinder();
            InitializeComponent();
            inputBox.TextChanged += Display;
        }

        public void Display(object sender, EventArgs e) => outputBox.Text = Finder.GetSuggestions(inputBox.Text).ResultsToString();
        
    }
}
