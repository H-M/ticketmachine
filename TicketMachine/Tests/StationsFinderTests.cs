﻿using System;
using System.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using TicketMachine.DStructures;

namespace TicketMachine.Tests
{
    [TestFixture]
    class StationsFinderTests
    {
        public static StationsFinder newFinder = new StationsFinder();
        public static Suggestions result;
        public static char[] keys = newFinder.mappedStations.Keys.ToArray();
        public static List<char> missingKeys = new List<char>();

        [Test]
        public void StationInitialInRightCategory()
        {
            Array.Sort(keys); // Did this because the NUnit does not work properly on my machine
            for (int iChar = 0; iChar < keys.Length; iChar++) {
                var stationsNamesByInitialChar = newFinder.GetSuggestions(keys[iChar].ToString()).Stations.ToArray();
                for (int iParam = 0; iParam < stationsNamesByInitialChar.Length; iParam++)
                {
                    Assert.AreEqual(keys[iChar], Char.ToLower(stationsNamesByInitialChar[iParam][0]));
                }
            }
        }

        [Test]
        public void ResultNotFoundWithNonValidStationNames()
        {
            string invalidStation1 = "Error 404 Station Not Found";
            string invalidStation2 = "Vacation Station";
            string invalidStation3 = "Inception Station";

            result = newFinder.GetSuggestions(invalidStation1);
            Assert.AreEqual(null, result.Stations);

            result = newFinder.GetSuggestions(invalidStation2);
            Assert.AreEqual(null, result.Stations);

            result = newFinder.GetSuggestions(invalidStation3);
            Assert.AreEqual(null, result.Stations);
        }

        [Test]
        public void EmptyResultsWhenEmptyInput()
        {
            result = newFinder.GetSuggestions("");
            Assert.AreEqual(null, result.Stations);
        }

        [Test]
        public void ResultNotFoundSpecialChars()
        {
            char[] specialChars = { '!', ',', '@', '$', '%', '^', '&', '*', '(', ')', ':', '+', '-', '#', '<', '>', ' ', '.', '/', '"', '=' };
            foreach (var specialChar in specialChars) {
                result = newFinder.GetSuggestions(specialChar.ToString());
                Assert.AreEqual(null, result.Stations);
            }
        }

        [Test]
        public void StationNameWithSpecialChars()
        {
            string stationNameWithSpecialChar1 = "!hendon";
            string stationNameWithSpecialChar2 = "Brixton*";
            string stationNameWithSpecialChar3 = "Bri%x&ton";

            result = newFinder.GetSuggestions(stationNameWithSpecialChar1);
            Assert.AreEqual(null, result.Stations);

            result = newFinder.GetSuggestions(stationNameWithSpecialChar2);
            Assert.AreEqual(null, result.Stations);

            result = newFinder.GetSuggestions(stationNameWithSpecialChar3);
            Assert.AreEqual(null, result.Stations);

        }
    }
}
