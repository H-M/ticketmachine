﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;

namespace TicketMachine.DStructures
{
    public class Suggestions : Interfaces.Suggestion
    {
        public Suggestions() { }
        public Suggestions(List<char> userInput, IEnumerable<string> searchResult)
        {
            this.NextLetter = userInput;
            this.Stations = searchResult;
        }
    }
}
