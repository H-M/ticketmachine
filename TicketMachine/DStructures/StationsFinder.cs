﻿using System;
using TicketMachine.Interfaces;
using System.Collections.Generic;

namespace TicketMachine.DStructures
{
    public class StationsFinder : Finder
    {
        public StationsFinder()
        {
            if(mappedStations == null)
                mappedStations = new Dictionary<char, HashSet<string>>();

            foreach (var station in StationsData.Data)
            {
                var formattedStationName = station.TrimStart();
                var key = Char.ToLower(formattedStationName[0]);
                if (mappedStations.ContainsKey(key))
                    mappedStations[key].Add(formattedStationName);

                else mappedStations.Add(key, new HashSet<string>() { formattedStationName });
            }
        }
    }
}
